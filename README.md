## tevi-helper
# functions

DecimalPrecision2,

encryptDataWithId,
decryptData,

sizeOf,
chunk,

fetchData,
headers,    

getTime,
parseSecondsToObjectHMS,

isFacebookApp,
isZaloApp,

isNumeric,
numberWithCommas,

getMobileOperatingSystem,
checkIsMobile,
isAndroid,
isIos,

removeDataFromLS,
setWithExpiryLS,
getWithExpiryLS,
saveToCookies,
saveDataToSession,
getDataFromSession,
removeDataFromSession,

formatNumber2Digit,

removeUnicode,

getCountryNameFromCountryCode
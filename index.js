import AES from 'crypto-js/aes.js';
import Utf8 from 'crypto-js/enc-utf8.js';
import axios from 'axios';
import Cookies from 'js-cookie';

const MAX_DATA_LENGTH = 50;

const typeSizes = {
    "undefined": () => 0,
    "boolean": () => 4,
    "number": () => 8,
    "string": item => 2 * item.length,
    "object": item => !item ? 0 : Object
        .keys(item)
        .reduce((total, key) => sizeOf(key) + sizeOf(item[key]) + total, 0)
};

const sizeOf = value => typeSizes[typeof value](value);

const chunk = (arr, size = MAX_DATA_LENGTH) => {
    let subArrayCount = arr.length / size;
    let res = [];
    for (let i = 0; i < subArrayCount; i++) {
        let from = size * i;
        let to = (size * (1 + i));
        let sliced = arr.slice(from, to);
        res.push(sliced);
    }
    return res;
}

const DecimalPrecision2 = (function () {
    if (Number.EPSILON === undefined) {
        Number.EPSILON = Math.pow(2, -52);
    }
    if (Math.trunc === undefined) {
        Math.trunc = function (v) {
            return v < 0 ? Math.ceil(v) : Math.floor(v);
        };
    }
    var powers = [
        1e0, 1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7,
        1e8, 1e9, 1e10, 1e11, 1e12, 1e13, 1e14, 1e15,
        1e16, 1e17, 1e18, 1e19, 1e20, 1e21, 1e22
    ];
    var intpow10 = function (power) {
        if (power < 0 || power > 22) {
            return Math.pow(10, power);
        }
        return powers[power];
    };
    var isRound = function (num, decimalPlaces) {
        //return decimalPlaces >= 0 &&
        //    +num.toFixed(decimalPlaces) === num;
        var p = intpow10(decimalPlaces);
        return Math.round(num * p) / p === num;
    };
    var decimalAdjust = function (type, num, decimalPlaces) {
        if (type !== 'round' && isRound(num, decimalPlaces || 0))
            return num;
        var p = intpow10(decimalPlaces || 0);
        var n = (num * p) * (1 + Number.EPSILON);
        return Math[type](n) / p;
    };
    return {
        // Decimal round (half away from zero)
        round: function (num, decimalPlaces) {
            return decimalAdjust('round', num, decimalPlaces);
        },
        // Decimal ceil
        ceil: function (num, decimalPlaces) {
            return decimalAdjust('ceil', num, decimalPlaces);
        },
        // Decimal floor
        floor: function (num, decimalPlaces) {
            return decimalAdjust('floor', num, decimalPlaces);
        },
        // Decimal trunc
        trunc: function (num, decimalPlaces) {
            return decimalAdjust('trunc', num, decimalPlaces);
        },
        // Format using fixed-point notation
        toFixed: function (num, decimalPlaces) {
            return decimalAdjust('round', num, decimalPlaces).toFixed(decimalPlaces);
        }
    };
})();

// crypto-js
const encryptDataWithId = (stringInput, id) => {
    if (id) {
        try {
            return AES.encrypt(stringInput, String(id)).toString();
        } catch (error) {
            console.error('encryptData error', JSON.stringify(error));
            return '';
        }
    }
    return '';
}

const decryptData = (stringInput, passPhase) => {
    if (passPhase) {
        const bytes = AES.decrypt(stringInput, String(passPhase));
        const originalText = bytes.toString(Utf8);
        return originalText;
    }
    return '';
}


// fetch data support
const createShortLinkGoogleFirebase = async (domainUriPrefix, androidPackageName, androidMinPackageVersionCode, iosBundleId, iosAppStoreId, url, key, metaTag = {}) => {
    const data = JSON.stringify({
        dynamicLinkInfo: {
            domainUriPrefix: domainUriPrefix,
            link: url,
            androidInfo: {
                androidPackageName: androidPackageName,
                androidMinPackageVersionCode: androidMinPackageVersionCode,
            },
            iosInfo: {
                iosBundleId: iosBundleId,
                iosAppStoreId: iosAppStoreId,
            },
            socialMetaTagInfo: metaTag,
            navigationInfo: {
                enableForcedRedirect: true,
            },
        },
    });

    try {
        const res = await axios({
            method: 'post',
            url: `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${key}`,
            headers: {
                'Content-Type': 'application/json',
            },
            data
        });
        return res.data.shortLink;
    } catch (error) {
        console.log(error);
        return url;
    }
}

const fetchData = async (data) => {
    return await axios(data);
}

const headers = (headers) => {
    Object.keys(headers).forEach((key) => {
        if (!headers[key]) {
            delete headers[key]
        }
    })

    return headers
}

// localStorage
const removeDataFromLS = (key) => {
    return localStorage.removeItem(key)
}

const setWithExpiryLS = (key, value, ttl) => {
    const now = new Date()
    const item = {
        value,
        expiry: now.getTime() + ttl,
    }

    localStorage.setItem(key, JSON.stringify(item))
}

const getWithExpiryLS = (key) => {
    const itemStr = localStorage.getItem(key)
    if (!itemStr) {
        return null
    }
    const item = JSON.parse(itemStr)
    const now = new Date()
    if (now.getTime() > item.expiry) {
        localStorage.removeItem(key)
        return null
    }
    return item.value
}

// cookies
const saveToCookies = (key, value, config = {
    expires: 7,
}) => {
    if (Cookies.get(key)) {
        Cookies.remove(key);
    }

    Cookies.set(key, JSON.stringify(value), config);
}

// sessionStorage
const saveDataToSession = (key, value) => {
    sessionStorage.setItem(key, JSON.stringify(value));
}

const getDataFromSession = (key) => {
    if (sessionStorage.getItem(key)) {
        return JSON.parse(sessionStorage.getItem(key));
    }
    return null;
}

const removeDataFromSession = (key) => {
    sessionStorage.removeItem(key);
}

// social app checking
const isFacebookApp = (ua) => {
    return (
        ua.indexOf('FBAN') > -1 ||
        ua.indexOf('FBAV') > -1 ||
        ua.indexOf('Instagram') > -1
    )
}

const isZaloApp = (ua) => {
    return ua.indexOf('Zalo') > -1 || ua.indexOf('ZALO') > -1;
}

// device process
const getMobileOperatingSystem = (ua) => {
    if (/windows phone/i.test(ua)) {
        return 'Windows Phone';
    }

    if (/android/i.test(ua)) {
        return 'Android';
    }

    if (/iPad|iPhone|iPod/.test(ua) && !window.MSStream) {
        return 'iOS';
    }

    return 'unknown';
}

const checkIsMobile = (ua) => {
    if (
        /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
            ua
        )
    ) {
        return true;
    }
    return false;
}

const isAndroid = (ua) => {
    return ua.toLowerCase().indexOf('android') > -1;
}

const isIos = (ua) => {
    return /iPad|iPhone|iPod/.test(ua);
}

// string process
const removeUnicode = (str) => {
    let newStr = str;
    newStr = newStr.toLowerCase();
    newStr = newStr.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    newStr = newStr.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    newStr = newStr.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    newStr = newStr.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    newStr = newStr.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    newStr = newStr.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    newStr = newStr.replace(/đ/g, 'd');
    newStr = newStr.replace(
        // eslint-disable-next-line no-useless-escape
        /!|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,
        '-'
    );

    newStr = newStr.replace(/-+-/g, '-'); // thay thế 2- thành 1-
    // eslint-disable-next-line no-useless-escape
    newStr = newStr.replace(/^\-+|\-+$/g, '');

    return newStr;
}

// number process
const isNumeric = (n) => {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

const numberWithCommas = (number) => {
    if (typeof number !== 'number' && typeof number !== 'string') {
        return ''
    }
    if (!isNumeric(number)) {
        return '';
    }
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

const formatNumber2Digit = (number, locale = 'en-US', minimumIntegerDigits = 2, useGrouping = false) => {
    return number.toLocaleString(locale, { minimumIntegerDigits, useGrouping });
}

// time process
const getTime = (timestamp, offset) => {
    const d = new Date(timestamp)
    const localTime = d.getTime()
    const localOffset = d.getTimezoneOffset() * 60000
    const utc = localTime + localOffset
    const nd = new Date(utc + 3600000 * offset)
    return nd
}

const parseSecondsToObjectHMS = (seconds) => {
    if (seconds > 0) {
        let hour = 0;
        let minute = 0;
        let second = 0;
        if (seconds > 3600) {
            hour = Math.floor(seconds / 3600);
        }
        if (seconds > 60) {
            minute = Math.floor((seconds - hour * 3600) / 60);
        }
        second = Math.floor(seconds - hour * 3600 - minute * 60);
        return {
            hour,
            minute,
            second,
        };
    }

    return {};
}

//country
const getCountryNameFromCountryCode = (countryCode, language = 'en') => {
    const regionNames = new Intl.DisplayNames([language], { type: 'region' });
    return regionNames.of(countryCode);
}

export default {
    DecimalPrecision2,

    encryptDataWithId,
    decryptData,

    sizeOf,
    chunk,

    createShortLinkGoogleFirebase,
    fetchData,
    headers,    

    getTime,
    parseSecondsToObjectHMS,

    isFacebookApp,
    isZaloApp,

    isNumeric,
    numberWithCommas,

    getMobileOperatingSystem,
    checkIsMobile,
    isAndroid,
    isIos,

    removeDataFromLS,
    setWithExpiryLS,
    getWithExpiryLS,
    saveToCookies,
    saveDataToSession,
    getDataFromSession,
    removeDataFromSession,

    formatNumber2Digit,

    removeUnicode,

    getCountryNameFromCountryCode
}